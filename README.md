# DMD assigment 3
## Where sql dump and queries are?
- [src/main/resources/sql](https://gitlab.com/innofba/dmd-taxi/tree/master/src/main/resources/sql)
## How to test requests?
- Go [here](https://dmd-taxi.herokuapp.com/)
- Choose query
- Fill parameters (some are prefilled)
- Click "Send request"
## Where is endpoints documentation?
- We use swagger for endpoints documentation
- Go [here](https://dmd-taxi.herokuapp.com/swagger-ui.html#/rest45ctrl)
- Choose endpoint
- Fill required parameters (e.g. user_id) 
- Click "Try it out!"
## Note
- All dates should be in format: `DD/MM/YYYY`
- We use heroku free plan, so we don't have much test data due to limits, also 
because of free plan first request might take up to 1 minute to process
- Our taxi works since 23/08/2018
- We only store coordinates for location, because all other info (city, postal code...) can be 
inferred from it using third-party apis