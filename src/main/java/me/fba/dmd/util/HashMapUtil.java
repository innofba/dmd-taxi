package me.fba.dmd.util;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HashMapUtil {

    public static <K, V> Map<K, V> map(MapEntry<K, V>... entries) {
        return Stream.of(entries)
                .collect(Collectors.toMap(e -> e.key, e -> e.value));
    }

    public static <K, V> MapEntry<K, V> e(K key, V value) {
        return new MapEntry<>(key, value);
    }

    public static class MapEntry<K, V> {
        K key;
        V value;

        private MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }
}
