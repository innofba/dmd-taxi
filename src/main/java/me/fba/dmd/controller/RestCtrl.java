package me.fba.dmd.controller;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import static java.nio.charset.StandardCharsets.UTF_8;
import static me.fba.dmd.util.HashMapUtil.e;
import static me.fba.dmd.util.HashMapUtil.map;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/query")
public class RestCtrl implements InitializingBean {

    private final NamedParameterJdbcTemplate jdbc;

    @Autowired
    public RestCtrl(NamedParameterJdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Integer dbNumber = jdbc.queryForObject("SELECT count(*) FROM pg_catalog.pg_tables", map(), Integer.class);

        if (dbNumber > 80)
            return;

        ScriptUtils.executeSqlScript(jdbc.getJdbcTemplate().getDataSource().getConnection(),
                new ClassPathResource("/sql/dump.sql"));
    }

    @GetMapping("/1")
    public List<Map<String, Object>> first(@RequestParam("user_id") Long userId,
                                           @RequestParam("licence_plate") String licencePlate,
                                           @RequestParam("car_colour") String carColour,
                                           @RequestParam("date") String date) {
        return jdbc.queryForList(readFile("/sql/1.sql"),
                map(e("userId", userId), e("licencePlate", licencePlate), e("carColour", carColour), e("date", date)));
    }

    @GetMapping("/2")
    public Map<String, Object> second(@RequestParam("date") String date) {
        return jdbc.queryForMap(readFile("/sql/2.sql"), map(e("date", date)));
    }

    @GetMapping("/3")
    public Map<String, Object> third(@RequestParam("date") String date) {
        return jdbc.queryForMap(readFile("/sql/3.sql"), map(e("date", date)));
    }

    @GetMapping("/4")
    public Map<String, Object> forth(@RequestParam("user_id") long userId) {
        return jdbc.queryForMap(readFile("/sql/4.sql"), map(e("userId", userId)));
    }

    @GetMapping("/5")
    public Map<String, Object> fifth(@RequestParam("date") String date) {
        return jdbc.queryForMap(readFile("/sql/5.sql"), map(e("date", date)));
    }

    @GetMapping("/6")
    public List<Map<String, Object>> sixth() {
        return jdbc.queryForList(readFile("/sql/6.sql"), map());
    }

    @GetMapping("/7")
    public List<Map<String, Object>> seventh() {
        return jdbc.queryForList(readFile("/sql/7.sql"), map());
    }

    @GetMapping("/8")
    public List<Map<String, Object>> eighth(@RequestParam("start_date") String startDate,
                                            @RequestParam("end_date") String endDate) {
        return jdbc.queryForList(readFile("/sql/8.sql"), map(e("startDate", startDate), e("endDate", endDate)));
    }

    @GetMapping("/9")
    public List<Map<String, Object>> ninth() {
        return jdbc.queryForList(readFile("/sql/9.sql"), map());
    }

    @GetMapping("/10")
    public Map<String, Object> tenth() {
        return jdbc.queryForMap(readFile("/sql/10.sql"), map());
    }

    private static String readFile(String file) {
        try {
            InputStream resource = RestCtrl.class.getResourceAsStream(file);
            return IOUtils.toString(resource, UTF_8).replace('\n', ' ');
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
