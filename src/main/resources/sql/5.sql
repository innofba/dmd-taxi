select avg(distance) as avg_distance,
       avg(duration) as avg_duration
from "order"
where date :: date = to_date(:date, 'DD/MM/YYYY')