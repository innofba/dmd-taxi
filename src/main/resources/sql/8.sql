select o.user_id as user_id, sum((select count(*)
                       from charge_log cl
                       where cl.car_id = o.car_id
                         and cl.start_date :: date = o.date :: date)) as amount
from "order" o
where o.date between to_date(:startDate, 'DD/MM/YYYY') and to_date(:endDate, 'DD/MM/YYYY')
group by o.user_id