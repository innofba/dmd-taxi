select count(order_id) = count(distinct order_id) as nothing_doubled
from income_statement
where order_id in (select id
                   from "order"
                   where user_id = :userId
                     and date :: date between CURRENT_DATE - INTERVAL '1 months' and CURRENT_DATE)