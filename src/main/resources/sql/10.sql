select *, (select avg(a.sum)
           from (select sum(rl.cost) as sum
                 from repair_log rl
                        inner join timeslot ts on rl.timeslot_id = ts.id
                 where rl.car_id = c.id
                 group by ts.from_time :: date) a)
            +
          (select avg(b.sum)
           from (select sum(cl.cost) as sum
                 from charge_log cl
                 where cl.car_id = c.id
                 group by cl.start_date :: date) b) as maintain_cost
from car c
order by maintain_cost desc NULLS LAST
limit 1