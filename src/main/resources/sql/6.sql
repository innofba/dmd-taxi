select array_to_string(array_agg(case when loc = 'pickup' then loc_id end), ',')      as pickup_location_ids,
       array_to_string(array_agg(case when loc = 'destination' then loc_id end), ',') as destination_location_ids,
       time
from ((select pickup_location_id as loc_id, count(*) as freq, 'pickup' as loc, 'morning' as time
       from "order"
       where extract(hour from date) between 7 and 10
       group by pickup_location_id
       order by freq desc
       limit 3)
      union
      (select destination_location_id as loc_id, count(*) as freq, 'destination' as loc, 'morning' as time
       from "order"
       where extract(hour from date) between 7 and 10
       group by destination_location_id
       order by freq desc
       limit 3)
      union (select pickup_location_id as loc_id, count(*) as freq, 'pickup' as loc, 'afternoon' as time
             from "order"
             where extract(hour from date) between 12 and 14
             group by pickup_location_id
             order by freq desc
             limit 3)
      union
      (select destination_location_id as loc_id, count(*) as freq, 'destination' as loc, 'afternoon' as time
       from "order"
       where extract(hour from date) between 12 and 14
       group by destination_location_id
       order by freq desc
       limit 3)
      union (select pickup_location_id as loc_id, count(*) as freq, 'pickup' as loc, 'evening' as time
             from "order"
             where extract(hour from date) between 17 and 19
             group by pickup_location_id
             order by freq desc
             limit 3)
      union
      (select destination_location_id as loc_id, count(*) as freq, 'destination' as loc, 'evening' as time
       from "order"
       where extract(hour from date) between 17 and 19
       group by destination_location_id
       order by freq desc
       limit 3)) freqs_loc
group by time