select *
from car
where licence_plate like :licencePlate
  and colour = :carColour
  and id in (select car_id from "order" where user_id = :userId
                                     and date :: date = to_date(:date, 'DD/MM/YYYY'))