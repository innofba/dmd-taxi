select (select count(distinct car_id)
        from "order"
        where extract(hour from date) between 7 and 10
          and date between to_date(:date, 'DD/MM/YYYY') and to_date(:date, 'DD/MM/YYYY') + interval '1 week') * 1.0 /
       (select count(*) from car) as morning,
       (select count(distinct car_id)
        from "order"
        where extract(hour from date) between 12 and 14
          and date between to_date(:date, 'DD/MM/YYYY') and to_date(:date, 'DD/MM/YYYY') + interval '1 week') * 1.0 /
       (select count(*) from car) as afternoon,
       (select count(distinct car_id)
        from "order"
        where extract(hour from date) between 17 and 19
          and date between to_date(:date, 'DD/MM/YYYY') and to_date(:date, 'DD/MM/YYYY') + interval '1 week') * 1.0 /
       (select count(*) from car) as evening