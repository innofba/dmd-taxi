select car_id, count(*) as took_orders
from "order"
where date :: date between CURRENT_DATE - INTERVAL '3 months' and CURRENT_DATE
group by car_id
order by took_orders
limit (select ((select count(*) from car) * 0.1) :: int);