select a.workshop_id,
       cpt.car_part_type,
       (select avg(r.sum) as sum
        from (select sum(wo.amount) as sum
              from workshop_order wo
              where wo.car_part_type_id = a.car_part_type_id
                and wo.workshop_id = a.workshop_id
              group by extract(week from wo.order_date) + extract(year from wo.order_date)) r) as weekly_avg
from (select e.workshop_id, e.car_part_type_id, sum(e.amount) as sum
      from workshop_order e
      group by workshop_id, car_part_type_id) a
       join (select c.workshop_id, max(c.sum) as max
             from (select d.workshop_id, d.car_part_type_id, sum(d.amount) as sum
                   from workshop_order d
                   group by d.workshop_id, d.car_part_type_id) c
             group by c.workshop_id) b on a.workshop_id = b.workshop_id and a.sum = b.max
       inner join car_part_type cpt on cpt.id = a.car_part_type_id